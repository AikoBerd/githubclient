package com.example.githubclient.data

import com.example.githubclient.domain.model.Item
import retrofit2.http.GET
import retrofit2.http.Path

interface Service {
    @GET("orgs/{organization}/repos")
    suspend fun getRepositories(@Path("organization") organization: String): List<Item>

    @GET("repos/{organization}/{repoName}")
    suspend fun getRepositoryDetails(
        @Path("organization") organization: String,
        @Path("repoName") repoName: String
    ): Item
}