package com.example.githubclient.data

import com.example.githubclient.domain.model.Item

object Repository {

    private val service = RetrofitInstance.getService()

    suspend fun getRepositories(organization: String): List<Item> {
        return service.getRepositories(organization)
    }

    suspend fun getRepositoryDetails(organization: String, repoName: String): Item {
        return service.getRepositoryDetails(organization, repoName)
    }
}

