package com.example.githubclient.domain.model

data class Item(
    val name: String,
    val description: String?,
    val forks: Int,
    val watchers: Int,
    val open_issues: Int,
    val full_name: String?
)