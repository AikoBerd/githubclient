package com.example.githubclient.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.example.githubclient.R
import com.example.githubclient.domain.model.Item

class DetailFragment : Fragment() {

    private lateinit var nameTextView: TextView
    private lateinit var descriptionTextView: TextView
    private lateinit var forksTextView: TextView
    private lateinit var watchersTextView: TextView
    private lateinit var issuesTextView: TextView
    private lateinit var parentTextView: TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var viewModel: ItemViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_detail, container, false)

        viewModel = ViewModelProvider(this)[ItemViewModel::class.java]

        viewModel.itemDetailMutableLiveData.observe(viewLifecycleOwner) { repositoryDetails ->
            displayRepositoryDetails(repositoryDetails)
        }

        viewModel.loading.observe(viewLifecycleOwner) { isLoading ->
            progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        }

        nameTextView = view.findViewById(R.id.nameTextView)
        descriptionTextView = view.findViewById(R.id.descriptionTextView)
        forksTextView = view.findViewById(R.id.forksTextView)
        watchersTextView = view.findViewById(R.id.watchersTextView)
        issuesTextView = view.findViewById(R.id.issuesTextView)
        parentTextView = view.findViewById(R.id.parentTextView)
        progressBar = view.findViewById(R.id.progressBar)

        val organization = arguments?.getString(ItemFragment.ORGANIZATION_KEY, "") ?: ""
        val repoName = arguments?.getString(ItemFragment.REPO_NAME_KEY, "") ?: ""

        viewModel.getRepositoryDetails(organization, repoName)
        return view
    }

    private fun displayRepositoryDetails(itemDetails: Item) {
        nameTextView.text = itemDetails.name
        descriptionTextView.text = itemDetails.description ?: "No description"
        forksTextView.text = itemDetails.forks.toString()
        watchersTextView.text = itemDetails.watchers.toString()
        issuesTextView.text = itemDetails.open_issues.toString()
        parentTextView.text = itemDetails.full_name ?: "N/A"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val activity = requireActivity() as AppCompatActivity
        activity.onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val navController = Navigation.findNavController(view)
                navController.navigate(R.id.action_fragment2_to_fragment1)
            }
        })
    }
}