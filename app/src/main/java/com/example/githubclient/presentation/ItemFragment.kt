package com.example.githubclient.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.githubclient.R
import com.example.githubclient.adapter.ItemAdapter
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class ItemFragment : Fragment() {

    private val adapter = ItemAdapter()
    private lateinit var viewModel: ItemViewModel
    private lateinit var progressBar: ProgressBar

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_item, container, false)

        viewModel = ViewModelProvider(this)[ItemViewModel::class.java]

        val itemRV = view.findViewById<RecyclerView>(R.id.rv)
        val layoutManager = LinearLayoutManager(requireContext())
        itemRV.layoutManager = layoutManager
        itemRV.adapter = adapter

        viewModel.itemMutableLiveData.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }
        viewModel.loading.observe(viewLifecycleOwner) { isLoading ->
            progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                //collect
                viewModel.showToast.onEach {
                    Toast.makeText(
                        requireContext(),
                        it,
                        Toast.LENGTH_SHORT
                    ).show()
                }.launchIn(this)

            }
        }

        adapter.onItemClick = { item->
            val bundle = Bundle().apply {
                putString(ORGANIZATION_KEY, item.full_name?.substringBefore("/"))
                putString(REPO_NAME_KEY, item.name)
            }
            val navController = Navigation.findNavController(view)
            navController.navigate(R.id.action_fragment1_to_fragment2, bundle)
        }

        progressBar = view.findViewById(R.id.progressBar)

        val organizationEditText = view.findViewById<EditText>(R.id.organizationEditText)
        val searchButton = view.findViewById<Button>(R.id.searchBtn)

        searchButton.setOnClickListener {
            val organization = organizationEditText.text.toString().trim()
            if (organization.isNotEmpty()) {
                viewModel.getRepositories(organization)
            }
        }

        return view
    }

    companion object{
        const val ORGANIZATION_KEY = "organization"
        const val REPO_NAME_KEY = "repoName"
    }
}