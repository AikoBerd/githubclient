package com.example.githubclient.presentation

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.*
import com.example.githubclient.data.Repository
import com.example.githubclient.domain.model.Item
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

class ItemViewModel(application: Application) : AndroidViewModel(application){
    private val repository = Repository

    private val _itemMutableLiveData = MutableLiveData<List<Item>>()
    val itemMutableLiveData: LiveData<List<Item>> = _itemMutableLiveData

    private val _itemDetailMutableLiveData = MutableLiveData<Item>()
    val itemDetailMutableLiveData: LiveData<Item> = _itemDetailMutableLiveData

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

//single live event

    private val _showToast = MutableSharedFlow<String>()
    val showToast = _showToast.asSharedFlow()

    val exampleFlow = flow {
        emit(1)
    }

    fun getRepositories(organization: String) {
        _loading.value = true
        viewModelScope.launch {
            try {
                val repositories = repository.getRepositories(organization)
                _itemMutableLiveData.value = repositories
                if (repositories.isEmpty()) {
//                    Toast.makeText(
//                        getApplication(),
//                        "No repos found for organization $organization",
//                        Toast.LENGTH_SHORT
//                    ).show()

                    _showToast.emit("No repos found for organization $organization")
                }
            } catch (e: Exception) {
//                Toast.makeText(
//                    getApplication(),
//                    "Failed to load repositories",
//                    Toast.LENGTH_SHORT
//                ).show()
                _showToast.emit("Failed to load repositories")

            }
            _loading.value = false
        }

    }

    fun getRepositoryDetails(organization: String, repoName: String) {
        _loading.value = true

        viewModelScope.launch {
            try {
                val repositoryDetails = repository.getRepositoryDetails(organization, repoName)
                _itemDetailMutableLiveData.value = repositoryDetails
            } catch (e: Exception) {
                Toast.makeText(
                    getApplication(),
                    "Failed to load repository details",
                    Toast.LENGTH_SHORT
                ).show()
            }
            _loading.value = false
        }
    }
}