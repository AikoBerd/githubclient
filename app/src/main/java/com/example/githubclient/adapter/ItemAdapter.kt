package com.example.githubclient.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.githubclient.R
import com.example.githubclient.domain.model.Item

class ItemAdapter : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

    private val itemList = mutableListOf<Item>()

    var onItemClick: ((Item) -> Unit)? = null

    fun submitList(newList: List<Item>) {
        itemList.clear()
        itemList.addAll(newList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(itemList[position])
    }

    override fun getItemCount(): Int = itemList.size

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private val name: TextView = view.findViewById(R.id.name)
        private val description: TextView = view.findViewById(R.id.description)

        fun onBind(list: Item){
            name.text = list.name
            description.text = list.description ?: view.context.getString(R.string.no_description)
            itemView.setOnClickListener {
                onItemClick?.invoke(list)
            }
        }
    }
}
